# DOCKER
 It's a program to use containers to develop and run applications.

## Purpose:-
 
*It's more lightweight than standard Containers and boots up in seconds. 

*The applications are packaged in a docker container which contains all the dependencies (libraries, packages) that are needed to deploy the application.

## BASIC TERMINOLOGIES:-

- **Docker image:-**  It contains a set of instructions required to deploy in docker environment.The container includes codes, libraries,variables for execution.
 It includes everything that is required to run the application when the container is deployed.

- **Containers:-**It is the docker image.The package of code which executes when deployed.

- **Docker hub:-**Similar like github but basically for docker containers and images.

![docker hub](https://www.javainuse.com/dock-8-11-min.JPG)

**Installing the Docker Engine:** [Official documentation: Installing Docker Engine](https://docs.docker.com/engine/install/ubuntu/)

### DOCKER IMAGE:- 

![DOCKER IMAGE](https://miro.medium.com/max/700/1*p8k1b2DZTQEW_yf0hYniXw.png)
 
It contains everything to run an application and is deployed as container.It can be deployed to any environment or sent in a container.
It includes codes, runtime, libraries, the dependencies, config files.

### DOCKER CONTAINER:-

![Docker Container](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)

It is a software that contains the code so that the application runs quickly and efficiently from one computing environment to another.
* running docker image
* can create multiple containers.

> Working of container:-
  Once the docker container containing docker images are deployed, it makes it easier to create an environment and run applications, they execute it.

### DOCKER ENGINE:-

![DOCKER ENGINE](https://docs.docker.com/engine/images/engine-components-flow.png)

* It is an open sourse containerisation technology for running and used to build applications.

* Acts as client server application.

* The dcker engine creates server side Daemon process that host images, containers,networks and storage volume

* It provides client side command line interface that enables us to interact with daemon through docker api.

## DOCKER WORKFLOW

![Docker Workflow](/assignments/summary/assets/docker-intro.png)

## DOCKER ARCHITECTURE
![Docker architecture](https://wiki.aquasec.com/download/attachments/2854889/Docker_Architecture.png?version=1&modificationDate=1520172700553&api=v2)

#### View all containers running on Docker's host

`docker ps`

#### Start any stopped containers

`docker start <container-name or container-id>`

#### Stop any running containers

`docker stop <container-name or beginning-of-container-id>` 

#### Create containers from Docker Images

`docker run <container-name>`

#### Delete a container

`docker rm <container-name>`

#### Download/pull Docker Images

`docker pull <image-author>/<image-name>`

#### Run Docker Image as a bash script

`docker run -ti <image-author>/<image-name> /bin/bash`

#### Copy any file inside Docker container with <container-id>

`docker cp <code-filename> <container-id>:/`

Furthermore, write a bash script say `install-dependencies.sh` to install all dependencies for successful execution of <code-filename> 

For instance, say <code-filename> was a basic python `.py` executable

`install-dependencies.sh` will include:

	apt update
	apt install python3

Now, copy the bash script into the same Docker container too

`docker cp install-dependencies.sh <container-id>:/`

#### Installing dependencies

 - Allow running bash script as executable 
 
 `docker exec -it <container-id> chmod +x install-dependencies.sh`
	
 - Install dependencies
 
 `docker exec -it <container-id> /bin/bash ./install-dependencies.sh`
	
#### Run program inside the container

Start the container

