# Git

 Git is version control software,open source project where coders can access the source code and change it accordingly

### Purpose
 
- *Free and open source :-One can easily download source codes and easily modify it according to their requirements.

- *The  chances  of  losing  data  are  very  rare  when  there  are  multiple  copies  of  it.

- *If we create a new branch,it  takes only a few seconds to create,delete,and merge branches.

### Basic terms:-

- **Repository:** It is the collection of files and folders that we use Git to track, often called as _Repo_.

- **Commit:** Saving the progress or changes.

- **Branch:** Usually,  a  branch  is createdto  work  on a new  feature.Once the
feauture is completed it is merged with the master branch.

- **Push:** Copies changes from the local repository to a remote one.Used to store changes permanently in the Git repository.

- **Merge:** It means integrating two branches together to become part of the primary codebase.

- **Clone:**It makes an exact copy of online repository in the local machine.

- **Fork:** It's a lot like cloning, allows us to duplicate an existing repo under our username.

### The Git Workflow:

It includes three prime states:

>**Working directory:**Constitutes all changes we make in our local source code on the repository.

>**Staging area:** staged files are stored in this tree of the local workspace.The staging area, that's where files are going to be a part of your next commit.

>**Local repo:** All committed files go to this tree of our workspace.
 
Now, pushing the committed changes into your remote repository is essentially required. After all, this is the whole point of Git anyway.
hence, simply put, the **Remote Repo** so it's accessible to all team members.

![The Git Workflow](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)


####Git workflow
  It is basically how to use git and it's mechanism
 
* Create a branch:- It is the part of repository but diverges from the main project.

* Checkout:- It is used to switch between branches in a repo.

* Next to clone:-It is used to copy the same repo in the local virtual machine.

* Make necassary changes.

* add files 

* Commit changes to local repository using git commit-m

* Push changes to remote using git push.


##### Cloning an existing repository

`git clone <link-to-repository>`

##### List branches

`git branch`

##### Switching to an existing branch

`git checkout <existing-branch-name>`

##### Creating a new branch

`git checkout -b <new-branch-name>`

##### Adding **all** changes from the working directory to the staging area

`git add .`

##### Committing **all** changes from the staging area to the local repo

`git commit -sm "<commit-message>"`

##### Pushing **all** changes from the local repo to an existing branch in the remote repo

`git push origin <existing-branch-name>`

##### Pull all changes from an existing branch in the remote repo to the local repo

`git pull origin <existing-branch-name>`

#### Compare the working directory with the staging area

`git diff`

#### Compare the working directory and the local repo

`git diff HEAD`




